function breakdownEmails(dataSet){
    if(!Array.isArray(dataSet) || dataSet.length === 0){
        throw new Error('Invalid Array Found');
    }
     emailBreakDown=[];
     for(let id =0; id<dataSet.length;id++){
        // find the dataset first from the given id
        const person= dataSet[id];
        //finding the email realted to that person
        const emailId= person.email;
        // dividing the email into parts  from '@'
        const emailParts=emailId.split('@');
        // again dividing the part 1  from  '.' and then storing it into array as firstName and lastName and the makeing the first letter of the name to 
        // upperCase and the slicing the remaining string to it.
        const [firstName,lastName] = emailParts[0].split('.').map(name => name.charAt(0).toUpperCase()+ name.slice(1));
       
        //  second part of the emailParts is the domain of the mail.
        const emailDomain=emailParts[1];
        //pushing all the values into array and returning it.
        emailBreakDown.push({firstName,lastName,emailDomain});

     }
     return emailBreakDown;

}
module.exports=breakdownEmails;