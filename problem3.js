function filterUniqueLanguages(dataSet) {
    if(!Array.isArray(dataSet) || dataSet.length === 0){
        throw new Error('Invalid Array Found');
    }
    const allLanguages = dataSet.reduce((languages, person) => {
        person.languages.forEach(language => {
            if (!languages.includes(language)) {
                languages.push(language);
            }
        });
        return languages;
    }, []);
    return allLanguages;
}
module.exports=filterUniqueLanguages;