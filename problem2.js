function groupProjectsByStatus(dataSet) {
    if(!Array.isArray(dataSet) || dataSet.length === 0){
        throw new Error('Invalid Array Found');
    }
    /*
    //creating an empty object
    const projectsByStatus = {};
    //running loop for all the id of person
    for (let id = 0; id < dataSet.length; id++) {
        // extractiong every person from the dataset
        const person = dataSet[id];
        // going inside every person details
        for (let projectId = 0; projectId < person.projects.length; projectId++) {
            // storing all the project details of that person in project
            const project = person.projects[projectId];
            // then if the project is not in the object then pushing it to object 
            if (!projectsByStatus[project.status]) {
                projectsByStatus[project.status] = [];
            }
            projectsByStatus[project.status].push(project.name);
        }
    }
    return projectsByStatus;
    */
    return dataSet.reduce((result , person )=>{
        person.projects.forEach(project =>{
            if(!result[project.status]){
                result[project.status]=[];
            }
            result[project.status].push(project.name);
        });
        return result;
    }, {});
}
module.exports= groupProjectsByStatus;