const dataSet = [
    {
      name: "John Doe",
      email: "john.doe@example.com",
      occupation: "Software Engineer",
      city: "New York",
      country: "USA",
      hobbies: ["Reading", "Cooking", "Photography"],
      languages: ["English", "Spanish"],
      skills: ["JavaScript", "Python", "SQL"],
      projects: [
        { name: "Project A", status: "Completed" },
        { name: "Project B", status: "Ongoing" },
        { name: "Project C", status: "In Progress" },
      ],
    },
    {
      name: "Jane Smith",
      email: "jane.smith@gmail.com",
      occupation: "Data Analyst",
      city: "London",
      country: "UK",
      hobbies: ["Painting", "Gardening", "Hiking"],
      languages: ["English", "French"],
      skills: ["R", "SQL", "Excel"],
      projects: [
        { name: "Data Analysis Project", status: "Completed" },
        { name: "Market Research Project", status: "Ongoing" },
        { name: "Customer Segmentation Project", status: "In Progress" },
      ],
    },
    {
      name: "Alice Johnson",
      email: "alice.johnson@yahoo.com",
      occupation: "Graphic Designer",
      city: "Paris",
      country: "France",
      hobbies: ["Drawing", "Travelling", "Playing Guitar"],
      languages: ["French", "English"],
      skills: ["Photoshop", "Illustrator", "InDesign"],
      projects: [
        { name: "Logo Redesign", status: "Completed" },
        { name: "Website Mockup", status: "Ongoing" },
        { name: "Print Ad Campaign", status: "In Progress" },
      ],
    },
    {
      name: "Michael Lee",
      email: "michael.lee@hotmail.com",
      occupation: "Marketing Manager",
      city: "Sydney",
      country: "Australia",
      hobbies: ["Surfing", "Cooking", "Playing Tennis"],
      languages: ["English", "Mandarin"],
      skills: ["Marketing Strategy", "Social Media Management", "Analytics"],
      projects: [
        { name: "Product Launch Campaign", status: "Completed" },
        { name: "Brand Awareness Campaign", status: "Ongoing" },
        { name: "Market Research Study", status: "In Progress" },
      ],
    },
    {
      name: "Sophia Wang",
      email: "sophia.wang@outlook.com",
      occupation: "UX Designer",
      city: "Shanghai",
      country: "China",
      hobbies: ["Sketching", "Reading", "Yoga"],
      languages: ["Chinese", "English"],
      skills: ["UI/UX Design", "Wireframing", "Prototyping"],
      projects: [
        { name: "Mobile App Redesign", status: "Completed" },
        { name: "Website User Testing", status: "Ongoing" },
        { name: "Dashboard Design", status: "In Progress" },
      ],
    },
  ];

  module.exports=dataSet;
  
  // Note: 
  // Use only HOF. 
  // Handle the errors properly. 
  // Code Identation should be proper. 
  // Follow the same folder structure as before.
  
  // Iterate over each object in the array and in the email breakdown the email and return the output as below:
  /*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */
  
  
  // Check the each person projects list and group the data based on the project status and return the data as below
  
  /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */
  
  
  // From the given data, filter all the unique langauges and return in an array
  
  
  
  
  